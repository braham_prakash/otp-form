import { Container, Grid, Typography } from "@mui/material";
import React, { useEffect, useContext } from "react";
import { useHistory } from "react-router";
import { AppContext } from "../App";

const Thankyou = () => {
  const { formData } = useContext(AppContext);
  const historyObj = useHistory();

  useEffect(() => {
    if (!formData["phone"]["value"].length) historyObj.push("/");
  }, [formData, historyObj]);

  return (
    <div className="thankyou-page">
      <Container maxWidth="md">
        <Grid
          container
          alignContent="center"
          alignItems="center"
          justifyContent="center"
          sx={{ height: "100vh" }}
        >
          <Grid item>
            <Typography
              color="green"
              align="center"
              variant="h2"
              sx={{
                fontSize: {
                  lg: "3.8rem",
                  md: "2rem",
                  sm: "1.8rem",
                  xs: "1.5rem",
                },
              }}
            >
              Thankyou your response has been recorded.
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Thankyou;
