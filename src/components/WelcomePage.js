import { AppBar, Container, Typography } from "@mui/material";
import React from "react";
import UserForm from "./UserForm";

const WelcomePage = () => {
  return (
    <div className="welcome-page">
      <AppBar position="sticky">
        <Typography
          color="#ffd100"
          sx={{
            background: "#202020",
            paddingBlock: "1rem",
            fontSize: {
              lg: "5rem",
              md: "4rem",
              sm: "3rem",
              xs: "2.9rem",
            },
          }}
          align="center"
          variant="h2"
          fontWeight="normal"
        >
          Welcome User
        </Typography>
      </AppBar>
      <Container maxWidth="lg" align="left">
        <Typography
          component="p"
          variant="h4"
          color="#222"
          align="center"
          sx={{
            padding: "0em",
            margin: "1em auto",
            fontSize: {
              lg: "2rem",
              md: "1.8rem",
              sm: "1.5rem",
              xs: "1.3rem",
            },
          }}
        >
          Please fill the form below:
        </Typography>

        <UserForm />
      </Container>
    </div>
  );
};

export default WelcomePage;
