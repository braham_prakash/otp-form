import { Button, Container, Grid, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

const NotFoundPage = () => {
  return (
    <div className="not-found-page">
      <Container>
        <Grid
          container
          alignItems="center"
          justifyContent="center"
          sx={{ width: "100%", minHeight: "50vh" }}
        >
          <Grid item>
            <Typography align="center" variant="h1" color="primary">
              Page Not Found
            </Typography>
            <br />
            <br />
            <Typography align="center">
              <Button size="large" color="warning" variant="contained">
                <Link
                  style={{
                    font: "inherit",
                    textDecoration: "inherit",
                    color: "inherit",
                  }}
                  to="/"
                >
                  Go back to home
                </Link>
              </Button>
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default NotFoundPage;
