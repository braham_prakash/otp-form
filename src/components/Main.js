import React from "react";
import { Switch, Route } from "react-router";
import NotFoundPage from "./NotFoundPage";
import OTPPage from "./OTPPage";
import Thankyou from "./Thankyou";
import WelcomePage from "./WelcomePage";

const Main = () => {
  return (
    <main>
      <Switch>
        <Route exact path="/thankyou" component={Thankyou} />
        <Route exact path="/otp-page" component={OTPPage} />
        <Route exact path="/home" component={WelcomePage} />
        <Route exact path="/index" component={WelcomePage} />
        <Route exact path="/" component={WelcomePage} />
        {/* <Route exact path="" component={WelcomePage} /> */}
        <Route path="*" component={NotFoundPage} />
      </Switch>
    </main>
  );
};

export default Main;
