import { Send } from "@mui/icons-material";
import {
  Button,
  Container,
  FormControl,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { AppContext } from "../App";
import { Debounce } from "../library/debounce";
const submitButtonDebounce = new Debounce();
const validationDebounce = new Debounce();

const UserForm = () => {
  const { formData, setFormData } = useContext(AppContext);

  const allCountryCodes = [
    "1",
    "7",
    "20",
    "27",
    "30",
    "31",
    "32",
    "33",
    "34",
    "36",
    "39",
    "40",
    "41",
    "43",
    "44",
    "45",
    "46",
    "47",
    "48",
    "49",
    "51",
    "52",
    "53",
    "54",
    "55",
    "56",
    "57",
    "58",
    "60",
    "61",
    "62",
    "63",
    "64",
    "65",
    "66",
    "81",
    "82",
    "84",
    "86",
    "90",
    "91",
    "92",
    "93",
    "94",
    "95",
    "98",
    "211",
    "212",
    "213",
    "216",
    "218",
    "220",
    "221",
    "222",
    "223",
    "224",
    "225",
    "226",
    "227",
    "228",
    "229",
    "230",
    "231",
    "232",
    "233",
    "234",
    "235",
    "236",
    "237",
    "238",
    "239",
    "240",
    "241",
    "242",
    "243",
    "244",
    "245",
    "246",
    "248",
    "249",
    "250",
    "251",
    "252",
    "253",
    "254",
    "255",
    "256",
    "257",
    "258",
    "260",
    "261",
    "262",
    "263",
    "264",
    "265",
    "266",
    "267",
    "268",
    "269",
    "290",
    "291",
    "297",
    "298",
    "299",
    "350",
    "351",
    "352",
    "353",
    "354",
    "355",
    "356",
    "357",
    "358",
    "359",
    "370",
    "371",
    "372",
    "373",
    "374",
    "375",
    "376",
    "377",
    "378",
    "379",
    "380",
    "381",
    "382",
    "383",
    "385",
    "386",
    "387",
    "389",
    "420",
    "421",
    "423",
    "500",
    "501",
    "502",
    "503",
    "504",
    "505",
    "506",
    "507",
    "508",
    "509",
    "590",
    "591",
    "592",
    "593",
    "595",
    "597",
    "598",
    "599",
    "670",
    "672",
    "673",
    "674",
    "675",
    "676",
    "677",
    "678",
    "679",
    "680",
    "681",
    "682",
    "683",
    "685",
    "686",
    "687",
    "688",
    "689",
    "690",
    "691",
    "692",
    "850",
    "852",
    "853",
    "855",
    "856",
    "880",
    "886",
    "960",
    "961",
    "962",
    "963",
    "964",
    "965",
    "966",
    "967",
    "968",
    "970",
    "971",
    "972",
    "973",
    "974",
    "975",
    "976",
    "977",
    "992",
    "993",
    "994",
    "995",
    "996",
    "998",
  ];

  const [disableSubmit, setDisableSubmit] = useState(true);

  const validate = (name, value, regex, regexinverted = false) => {
    if (
      !value.length || regexinverted ? !value.match(regex) : value.match(regex)
    ) {
      setFormData((prevData) => ({
        ...prevData,
        [name]: { ...prevData[name], hasError: true },
      }));
    } else {
      setFormData((prevData) => ({
        ...prevData,
        [name]: { ...prevData[name], hasError: false },
      }));
    }
  };

  const handleValidation = ({ name, value }) => {
    switch (name) {
      case "name":
        validate(name, value, /(?!\s)[^a-z]/gim);
        break;

      case "email":
        validate(name, value, /\S+@\S+\.\S+/gim, true);
        break;

      case "phone":
        validate(name, value, /\D/gim);
        break;

      default:
        console.log("default case ran");
        break;
    }
  };

  const handleFormFieldChange = (e) => {
    const { name, value } = e.target;

    setFormData((prevData) => ({
      ...prevData,
      [name]: { ...prevData[name], value },
    }));

    validationDebounce.debounce(() => {
      handleValidation({ name, value });
    }, 500);
  };

  const handlePhoneChange = (e) => {
    const { value } = e.target;

    if (value.length >= 11) {
      setFormData((prevData) => ({
        ...prevData,
        phone: {
          ...prevData["phone"],
          hasError: true,
          value,
          helperText: "Phone number cannot be more than 10 digts",
        },
      }));

      return;
    }
    handleFormFieldChange(e);
  };

  const handleCounryCodeChange = (e) => {
    const { value } = e.target;
    const name = "phone";

    setFormData((prevData) => ({
      ...prevData,
      [name]: { ...prevData[name], prefix: value },
    }));
  };

  const handleFormSubmit = (e) => {
    // e.preventDefault();

    const finalData = {};

    for (let item in formData) {
      if (item === "phone")
        finalData[
          item
        ] = `${formData[item]["prefix"]}${formData[item]["value"]}`;
      else finalData[item] = formData[item]["value"].trim();
    }

    console.log(finalData);
  };

  useEffect(() => {
    submitButtonDebounce.debounce(() => {
      const res = Object.keys(formData).some(
        (item) => formData[item]["hasError"] || !formData[item]["value"].length
      );

      setDisableSubmit(res);
    }, 100);
  }, [formData]);

  return (
    <div className="user-form">
      <Container className="form-container" maxWidth="sm">
        <form action="">
          <div className="form-item">
            <TextField
              fullWidth
              helperText={
                formData["name"]["hasError"] && formData["name"]["helperText"]
              }
              id="name"
              name="name"
              type="text"
              color="primary"
              label="Name"
              variant="outlined"
              onChange={handleFormFieldChange}
              error={formData["name"]["hasError"]}
              value={formData["name"]["value"]}
              required
            />
          </div>

          <div className="form-item">
            <TextField
              fullWidth
              helperText={
                formData["email"]["hasError"] && formData["email"]["helperText"]
              }
              id="email"
              name="email"
              type="email"
              color="primary"
              label="Email"
              variant="outlined"
              onChange={handleFormFieldChange}
              error={formData["email"]["hasError"]}
              value={formData["email"]["value"]}
              required
            />
          </div>

          <div className="form-item form-item-number">
            <FormControl required sx={{ width: "7rem" }}>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={formData["phone"]["prefix"]}
                onChange={handleCounryCodeChange}
              >
                {allCountryCodes.map((code) => (
                  <MenuItem key={code} value={code}>
                    {code}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <TextField
              helperText={
                formData["phone"]["hasError"] && formData["phone"]["helperText"]
              }
              fullWidth
              id="phone"
              name="phone"
              type="tel"
              color="primary"
              label="Phone"
              variant="outlined"
              onChange={handlePhoneChange}
              error={formData["phone"]["hasError"]}
              value={formData["phone"]["value"]}
              required
            />
          </div>

          <div className="form-item">
            <TextField
              fullWidth
              helperText={
                formData["message"]["hasError"] &&
                formData["message"]["helperText"]
              }
              id="message"
              name="message"
              type="text"
              multiline
              rows={4}
              color="primary"
              label="Message"
              variant="outlined"
              onChange={handleFormFieldChange}
              error={formData["message"]["hasError"]}
              value={formData["message"]["value"]}
              required
            />
          </div>

          <Typography component="p" color="#1f3b91">
            After successfull submission an OTP will be sent to your phone
            number
          </Typography>

          <div className="form-item">
            <Link
              to="/otp-page"
              onClick={handleFormSubmit}
              style={{
                pointerEvents: disableSubmit ? "none" : "all",
              }}
            >
              <Button
                variant="contained"
                color="success"
                disableElevation
                disabled={disableSubmit}
                endIcon={<Send />}
                fullWidth
                type="submit"
              >
                Submit
              </Button>
            </Link>
          </div>
        </form>
      </Container>
    </div>
  );
};

export default UserForm;
