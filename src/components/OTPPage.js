import { Send } from "@mui/icons-material";
import { Button, Container, TextField, Typography } from "@mui/material";
import React, { useContext, useState, useEffect, useRef } from "react";
import { Link, useHistory } from "react-router-dom";
import { AppContext } from "../App";
import { Debounce } from "../library/debounce";
const otpInputDebouce = new Debounce();

const OTPPage = () => {
  const { formData } = useContext(AppContext);
  const moblie = formData["phone"]["value"];
  const [message, setMessage] = useState("");
  const [otpGroup, setOtpGroup] = useState({});
  const [otpInput, setOtpInput] = useState("");
  const [otpHasError, setOtpHasError] = useState(false);
  const [messageHasError, setMessageHasError] = useState(false);
  const [showLoadingSpinner, setShowLoadingSpinner] = useState(false);
  const [showInput, setShowInput] = useState(true);
  const [resendOTP, setResendOTP] = useState(false);
  const OTPGroupRef = useRef(null);
  const historyObj = useHistory();

  const fetchOTP = async (mobile) => {
    let headerData = { App: "website_CMS", mobile };
    let res = await fetch("https://pristyntech.com/java/otp/getotp", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(headerData),
    });

    const data = await res.json();
    return data;
  };

  const verifyOTP = async (otp, mobile) => {
    let headerData = { otp, isInternal: false, mobile };
    let res = await fetch(
      "https://pristyntech.com/java/otp/verify_otpForWebsiteForm",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(headerData),
      }
    );

    const data = await res.json();
    return data;
  };

  const handleOTPButtonClick = () => {
    setShowLoadingSpinner(true);
    fetchOTP(moblie)
      .then(({ status, description }) => {
        setMessageHasError(() => status === "Failed");
        setMessage(() => description);
        setShowInput(true);
      })
      .catch((err) => {
        setMessageHasError(true);
        setMessage(() => err.message);
      })
      .finally(() => {
        setShowLoadingSpinner(false);
      });
  };

  const handleVerifyButtonClick = () => {
    setShowLoadingSpinner(true);
    verifyOTP(otpInput, moblie)
      .then((data) => {
        const { status, description } = data;
        setMessage(() => description);
        if (status === "Failed") {
          setResendOTP(true);
          setMessageHasError(true);
        } else {
          historyObj.push("/thankyou");
        }
      })
      .catch((err) => {
        setMessageHasError(true);
        setMessage(() => err.message);
      })
      .finally(() => {
        setShowLoadingSpinner(false);
      });
  };

  const handleOTPGroupChange = (e) => {
    const { name, value } = e.target;

    setOtpGroup((prevData) => ({
      ...prevData,
      [name]: value.slice(-1),
    }));

    const nextElement = document.querySelector(
      `.otp-input-group-container #otp-input-item-${Number(name) + 1}`
    );

    if (nextElement) nextElement.focus();
  };

  useEffect(() => {
    setOtpInput(() => {
      let res = [];

      for (let item in otpGroup) {
        res.push(otpGroup[item]);
      }

      return res.join("");
    });
  }, [otpGroup]);

  useEffect(() => {
    if (!moblie.length) historyObj.push("/");
    else {
      handleOTPButtonClick();
    }
  }, []);

  return (
    <div className="otp-page">
      <Container className="otp-container" maxWidth="sm">
        <Typography
          align="center"
          variant="h4"
          color="#444"
          sx={{ margin: "1em auto" }}
        >
          Otp sent to {moblie}
        </Typography>

        <Typography
          align="center"
          component="p"
          color="goldenrod"
          fontSize="1.5em"
          sx={{
            margin: "1em auto",
            color: messageHasError ? "orangered" : "green",
            transition: ".3s ease",
          }}
        >
          {message}
        </Typography>

        <div
          className="loading-spinner"
          style={{ filter: showLoadingSpinner ? "opacity(1)" : "opacity(0)" }}
        ></div>

        <Container maxWidth="xs">
          <div className="form-container">
            <form action="">
              {showInput ? (
                <>
                  <div ref={OTPGroupRef} className="otp-input-group-container">
                    {[...Array(4)]?.map((_, index) => (
                      <div className="form-item otp-input-item">
                        <TextField
                          variant="outlined"
                          id={`otp-input-item-${index}`}
                          name={index}
                          color="primary"
                          error={otpHasError}
                          required
                          value={otpGroup[index]}
                          onChange={handleOTPGroupChange}
                          sx={{ width: "4rem" }}
                        />
                      </div>
                    ))}
                    <Button
                      variant="contained"
                      color="success"
                      disableElevation
                      onClick={handleOTPButtonClick}
                      fullWidth
                    >
                      {"Resend"}
                    </Button>
                  </div>

                  <div className="form-item">
                    <Button
                      variant="contained"
                      color="success"
                      disableElevation
                      onClick={handleVerifyButtonClick}
                      endIcon={<Send />}
                      fullWidth
                    >
                      Verify OTP
                    </Button>
                  </div>
                </>
              ) : (
                <>
                  {/* <div className="form-item">
                    <Button
                      variant="contained"
                      color="success"
                      disableElevation
                      onClick={handleOTPButtonClick}
                      fullWidth
                    >
                      {resendOTP ? "Resend OTP" : "Request OTP"}
                    </Button>
                  </div> */}

                  {resendOTP && (
                    <div className="form-item">
                      <Link>
                        <Button
                          variant="contained"
                          color="warning"
                          disableElevation
                          fullWidth
                        >
                          Go Back
                        </Button>
                      </Link>
                    </div>
                  )}
                </>
              )}

              {resendOTP && (
                <>
                  {/* <div className="form-item">
                    <Button
                      variant="contained"
                      color="success"
                      disableElevation
                      onClick={handleOTPButtonClick}
                      fullWidth
                    >
                      {resendOTP ? "Resend OTP" : "Request OTP"}
                    </Button>
                  </div> */}

                  <div className="form-item">
                    <Link to="/">
                      <Button
                        variant="contained"
                        color="warning"
                        disableElevation
                        fullWidth
                      >
                        Go Back
                      </Button>
                    </Link>
                  </div>
                </>
              )}
            </form>
          </div>
        </Container>
      </Container>
    </div>
  );
};

export default OTPPage;
