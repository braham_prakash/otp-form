export class Debounce {
  debounceTimer;
  debounce(cb, delay = 1000) {
    clearTimeout(this.debounceTimer);
    this.debounceTimer = setTimeout(cb, delay);
  }
}
