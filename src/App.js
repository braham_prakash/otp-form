import "./css/app.css";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Main from "./components/Main";
import { BrowserRouter as Router } from "react-router-dom";
import React, { useState } from "react";
export const AppContext = React.createContext();
// import { makeStyles } from "@mui/styles";

// const useStyles = makeStyles({
//   formField: {
//     margin: ".5rem 0 !important",
//     display: "block !important",
//   },
// });

const myTheme = createTheme({
  palette: {
    primary: {
      main: "#FFD100",
      light: "#FFEE32",
      dark: "#F5C800",
      contrastText: "#D6D6D6",
    },
  },
  typography: {
    fontFamily: "'Quicksand', sans-serif",
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 600,
  },
});

function App() {
  const initialData = {
    name: {
      value: "",
      hasError: false,
      helperText: "Only alphabets and space are permitted ",
    },
    email: {
      value: "",
      hasError: false,
      helperText: "Please enter valid email",
    },
    phone: {
      value: "",
      prefix: "91",
      hasError: false,
      helperText: "Please enter valid phone",
    },
    message: {
      value: "",
      hasError: false,
      helperText: "Please enter valid message",
    },
  };

  const [formData, setFormData] = useState(initialData);

  return (
    <Router>
      <AppContext.Provider value={{ formData, setFormData }}>
        <ThemeProvider theme={myTheme}>
          <div className="App">
            <Main />
          </div>
        </ThemeProvider>
      </AppContext.Provider>
    </Router>
  );
}

export default App;
